//
//  ViewController.swift
//  Date_Picker_Example
//
//  Created by Admin on 29/06/18.
//  Copyright © 2018 Date_Picker_Example. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var datePicker : DatePicker? = nil

    @IBOutlet weak var dateShowLabel: UILabel!

    @IBOutlet weak var dateShowLabel2: UILabel!
    
    var senderTag : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func showDatePickerAction(_ sender: UIButton) {
        senderTag = 0
        dateFunction()
    }
    
    func dateFunction(){
        if datePicker == nil {
            datePicker = DatePicker(view: self.view, delegates: self)
        }
        datePicker?.show()
    }
    @IBAction func ShowDatePickerAction2(_ sender: UIButton) {
        senderTag = 1
        dateFunction()
    }
    
}

extension ViewController : DatePickerDelegate {
   
    func didSelectDate(datePicker: DatePicker, dateString: String) {
        if senderTag == 0{
            dateShowLabel.text = dateString
        }
        else{
            dateShowLabel2.text = dateString
        }
        
        print(senderTag)
        dateHiddenFunction()
    }
    
    func dateHiddenFunction(){
        self.datePicker?.hide()
        self.datePicker?.delegate = nil
        self.datePicker = nil
    }
    
    func didCancel(datePicker: DatePicker) {
        dateHiddenFunction()
    }
    
}

