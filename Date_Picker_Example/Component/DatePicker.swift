//
//  DatePickerClass.swift
//  Railway PMS
//
//  Created by Admin on 31/05/18.
//  Copyright © 2018 alinasoftware. All rights reserved.
//

import UIKit
protocol DatePickerDelegate {
    func didSelectDate(datePicker : DatePicker , dateString : String)
    func didCancel(datePicker : DatePicker)
}

class DatePicker: NSObject {

    var delegate : DatePickerDelegate? = nil
    
    weak var view :UIView? = nil
    var overlayView :UIView? = nil
     var doneButtonView :UIView? = nil

    var datePicker : UIDatePicker?

     init(view: UIView) {
        super.init()
        self.view = view
    }
    
    init(view: UIView , delegates : DatePickerDelegate) {
        super.init()
        self.view = view
        self.delegate = delegates
    }
    
    func show()
    {
        if datePicker == nil{
        datePicker = UIDatePicker()
            datePicker?.datePickerMode = UIDatePickerMode.date
            let pickerSize : CGSize = (datePicker?.sizeThatFits(CGSize.zero))!
            datePicker?.frame = CGRect(x: 0.0, y: (self.view?.frame.size.height)!  - 240 ,width: (self.view?.frame.size.width)!, height: 240)
            datePicker?.backgroundColor = UIColor.white
            doneButtonView = UIView()
            doneButtonView?.frame = CGRect(x: 0, y: (datePicker?.frame.origin.y)!  - 40 , width: (datePicker?.frame.size.width)!, height: 40)
            let toolbar = UIToolbar();
            toolbar.sizeToFit()
            
            //done button & cancel button
            let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.bordered, target: self, action: #selector(DatePicker.doneButtonAction))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.bordered, target: self, action: #selector(DatePicker.cancelButtonAction))
            toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)

            doneButtonView?.addSubview(toolbar)
            
        }
        var rect_overlay = (self.view?.frame)!
        rect_overlay.origin.y = 0
        overlayView = UIView(frame: (rect_overlay))
        overlayView?.backgroundColor = UIColor.init(white: 0, alpha: 0.2)
        self.view?.addSubview(overlayView!)
        self.view?.addSubview(datePicker!)
        self.view?.addSubview(doneButtonView!)

    }
    
    @objc func doneButtonAction()
    {
        
        let formatter = DateFormatter()
        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let myString = formatter.string(from: (datePicker?.date)!) // string purpose I add here
        // convert your string to date
        let yourDate = formatter.date(from: myString)
        //then again set the date format whhich type of output you need
        formatter.dateFormat = "yyyy-MM-dd"
        // again convert your date to string
        let myStringafd = formatter.string(from: yourDate!)
        
        print(myStringafd)

        self.delegate?.didSelectDate(datePicker: self, dateString: myStringafd)

    }
    @objc func cancelButtonAction()
    {
        self.delegate?.didCancel(datePicker: self)
    }
    func hide ()
    {
        overlayView?.removeFromSuperview()
        datePicker?.removeFromSuperview()
        doneButtonView?.removeFromSuperview()
    }
    
    deinit{
      delegate = nil
       view = nil
    }
    
}
